+++
title = "Haybot"

[extra]
link = "https://gitlab.com/haybot"
+++

Bot Discord écrit en Rust autour de la librairie asynchrone [Serenity](https://github.com/serenity-rs/serenity). C'est une démo de RPG textuel autocompilé/déployé sur [ASUS TinkerBoard](https://tinker-board.asus.com/product/tinker-board.html) utilisant Git comme base de données.