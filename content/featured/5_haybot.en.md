+++
title = "Haybot"

[extra]
link = "https://gitlab.com/haybot"
+++

Discord bot written in Rust using the asynchronous library [Serenity](https://github.com/serenity-rs/serenity). It is a demo of a self-compiled/deployed text-based RPG on [ASUS TinkerBoard](https://tinker-board.asus.com/product/tinker-board.html) using Git as a database.
