+++
title = "Embedded Rust on ESP32C3 Board, a Hands-on Quickstart Guide"

[extra]
link = "https://gitlab.com/cyril-marpaud/rust_esp_quickstart"
+++

Tutorial detailing the setup of a Rust development environment on **RISC-V ESP32C3** microcontroller in less than an hour, allowing the use of the **Rust Standard Library** as well as **Wi-Fi** and **Bluetooth** features of the MCU.