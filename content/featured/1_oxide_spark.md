+++
title = "OxidESPark"

[extra]
link = "https://gitlab.com/cyril-marpaud/oxide-spark"
+++

Bibliothèque Rust pour [Rust ESP Board](https://github.com/esp-rs/esp-rust-board) intégrant un microcontrôleur ESP32-C3 (RISC-V). Elle utilise le framework [ESP-IDF](https://github.com/esp-rs/esp-idf-sys) et fournit des outils pour construire facilement des applications interagissant avec le monde physique.
