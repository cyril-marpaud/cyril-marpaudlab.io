+++
title = "Unlocking Vec and HashMap on BBC Micro:Bit"

[extra]
link = "https://gitlab.com/cyril-marpaud/microbit_vec_hashmap"
+++

Tutorial detailing how to use Rust's Vec and HashMap collections on the BBC micro:bit's ARM Cortex-M4F microcontroller in under an hour by enabling the use of the Rust Standard Library ("std").
