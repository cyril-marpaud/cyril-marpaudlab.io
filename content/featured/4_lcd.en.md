+++
title = "I2C LCD"

[extra]
link = "https://gitlab.com/cyril-marpaud/i2c_lcd"
+++

Driver for HD44780 controller via I2C bus written in Rust, allowing control of an LCD screen with a concrete example on the [BBC micro:bit](https://microbit.org) (measurement and display of temperature via the onboard sensor).