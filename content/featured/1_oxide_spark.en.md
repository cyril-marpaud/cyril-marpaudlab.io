+++
title = "OxidESPark"

[extra]
link = "https://gitlab.com/cyril-marpaud/oxide-spark"
+++

Rust library for the [Rust ESP Board](https://github.com/esp-rs/esp-rust-board) embedding an ESP32-C3 microcontroller (RISC-V). It uses the [ESP-IDF](https://github.com/esp-rs/esp-idf-sys) framework and provides tools to easily build applications that interact with the physical world.
