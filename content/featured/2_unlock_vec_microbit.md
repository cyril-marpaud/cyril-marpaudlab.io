+++
title = "Unlocking Vec and HashMap on BBC Micro:Bit"

[extra]
link = "https://gitlab.com/cyril-marpaud/microbit_vec_hashmap"
+++

Tutoriel détaillant l'utilisation des collections Vec et HashMap de Rust sur le microcontrôleur ARM Cortex-M4F du BBC micro:bit en moins d'une heure en activant l'utilisation de la bibliothèque standard de Rust ("std").
