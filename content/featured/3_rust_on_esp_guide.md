+++
title = "Embedded Rust on ESP32C3 Board, a Hands-on Quickstart Guide"

[extra]
link = "https://gitlab.com/cyril-marpaud/rust_esp_quickstart"
+++

Tutoriel détaillant la mise en place d'un environnement de développement Rust sur microcontrôleur **RISC-V ESP32C3** en moins d'une heure permettant d'utiliser la **Bibliothèque Rust Standard** ainsi que les fonctionnalités **Wi-Fi** et **Bluetooth** du MCU.