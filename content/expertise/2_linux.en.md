+++
title = "Backend Linux"

[extra]
icons = ["fa-brands fa-linux"]
+++

My field of expertise covers development on Linux, whether it is application or embedded.