+++
title = "Systèmes Embarqués"

[extra]
icons = ["fa-solid fa-microchip"]
+++

Passioné par ce domaine, j'ai acquis plus de huit ans d'expérience dans l'industrie des systèmes embarqués.