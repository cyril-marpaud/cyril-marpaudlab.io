+++
title = "Rust"

[extra]
icons = ["fa-brands fa-rust"]
+++

Performance et modernité enfin accessibles dans un langage élégant et plébiscité !
