+++
title = "Expertise"
sort_by = "slug"

[extra]
card_type = "columns"
+++

Graduated from the **Electronics and Applied Physics** program with a major in **Signal, Control, and Telecommunications for Embedded Systems** at ENSICAEN in 2013, I have since worked with companies of various sizes, some of them world-renowned (*Intel*, *Airbus*, *Alstom*, *ABB*) and in various sectors (*Telecommunications*, *Energy*, *Aerospace*, *Railway*, *Cybersecurity*...).