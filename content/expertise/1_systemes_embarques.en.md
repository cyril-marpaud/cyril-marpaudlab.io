+++
title = "Embedded Systems"

[extra]
icons = ["fa-solid fa-microchip"]
+++

Passionate about this field, I have gained over eight years of experience in the embedded systems industry.